package com.example.jatistransproject;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jatistransproject.model.BusModel;
import com.example.jatistransproject.model.SeatModel;

import java.util.ArrayList;
import java.util.List;

public class SeatActivity extends AppCompatActivity {


    ArrayList<ImageView> seatList;
    TextView cost;
    int costPerTicket;
    Button done;
    BusModel thisBus = new BusModel();

    public static String KEY_PRICE = "harga";

    ArrayList<String> selectedSeats; //Selected by this user.

    ArrayList<SeatModel> filledSeats; //Already booked by other users.

    int busID;
    String emailid;
    String key;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seat);

        cost = findViewById(R.id.tv);
        done = findViewById(R.id.donebtn);

        seatList = new ArrayList<>();
        seatList.add((ImageView) findViewById(R.id.seat1));
        seatList.add((ImageView) findViewById(R.id.seat2));
        seatList.add((ImageView) findViewById(R.id.seat3));
        seatList.add((ImageView) findViewById(R.id.seat4));
        seatList.add((ImageView) findViewById(R.id.seat5));
        seatList.add((ImageView) findViewById(R.id.seat6));
        seatList.add((ImageView) findViewById(R.id.seat7));
        seatList.add((ImageView) findViewById(R.id.seat8));
        seatList.add((ImageView) findViewById(R.id.seat9));
        seatList.add((ImageView) findViewById(R.id.seat10));
        seatList.add((ImageView) findViewById(R.id.seat11));
        seatList.add((ImageView) findViewById(R.id.seat12));
        seatList.add((ImageView) findViewById(R.id.seat13));
        seatList.add((ImageView) findViewById(R.id.seat14));
        seatList.add((ImageView) findViewById(R.id.seat15));
        seatList.add((ImageView) findViewById(R.id.seat16));
        seatList.add((ImageView) findViewById(R.id.seat17));
        seatList.add((ImageView) findViewById(R.id.seat18));
        seatList.add((ImageView) findViewById(R.id.seat19));
        seatList.add((ImageView) findViewById(R.id.seat20));
        seatList.add((ImageView) findViewById(R.id.seat21));
        seatList.add((ImageView) findViewById(R.id.seat22));

        selectedSeats = new ArrayList<>();
        filledSeats = new ArrayList<>();

        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int i = 0; i < seatList.size(); i++) {
                    if (seatList.get(i).getId() == view.getId()) {

                        //Unselecting selected seat
                        if (isSelectedByUser(i + 1))
                        {
                            if (contains(selectedSeats, i + 1)) {
                                selectedSeats.remove(String.valueOf(i + 1));
                                seatList.get(i).setColorFilter(Color.parseColor("#000000"));
                            }
                            cost.setText(new StringBuilder(selectedSeats.size() * costPerTicket + ""));
//                            seatList.get(i).setColorFilter(Color.parseColor("#000000"));
//                            //Toast.makeText(SelectSeat.this, "Seat No. " + (i + 1) + " unselected", Toast.LENGTH_SHORT).show();
//                            int j = 0;
//                            while (j < selectedSeats.size())
//                            {
//                                if (selectedSeats.get(j).getSeatNo() == (i + 1))
//                                {
//                                    selectedSeats.remove(j);
//                                    break;
//                                }
//                            }
//                            cost.setText(new StringBuilder(selectedSeats.size()*costPerTicket+ ""));
                        }
                        //Selecting
                        else if (!isSelected((i + 1)))
                        {
                            seatList.get(i).setColorFilter(Color.parseColor("#2ecc71"));
                            selectedSeats.add(String.valueOf(i + 1));
                            Toast.makeText(SeatActivity.this, "Seat No. " + (i + 1) + " selected", Toast.LENGTH_SHORT).show();

                            cost.setText(new StringBuilder(selectedSeats.size() * costPerTicket + ""));
                        }
                        //Cannot select
                        else {
                            Toast.makeText(SeatActivity.this, "Seat already booked", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        };

        for (int i = 0; i < seatList.size(); i++) {
            seatList.get(i).setOnClickListener(onClickListener);
        }

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (selectedSeats.isEmpty()) {
                    Toast.makeText(SeatActivity.this, "No seat selected!", Toast.LENGTH_SHORT).show();
                } else {

                    for (int i = 0; i < selectedSeats.size(); i++) {
                        filledSeats.add(new SeatModel(selectedSeats.get(i)));
                    }

                    String newSeatsString = "";
                    for(int i = 0;i<filledSeats.size();i++)
                    {
                        newSeatsString = newSeatsString + filledSeats.get(i).getmSeat_no() + " ";
                    }

                    thisBus.setFilledSeats(newSeatsString.trim());

                    String seatString = "";

                    for (int i = 0; i < selectedSeats.size(); i++) {
                        seatString = seatString + " " + selectedSeats.get(i);
                    }

                }
            }
        });
    }

    boolean contains(List<String> seatModels, int seat) {

        for (String seatModel : seatModels) {
            //System.out.println(">>>>>>>>   " + seatModel);
            //System.out.println("seat>>>>>>>>   " + seat);
            if (seatModel.equalsIgnoreCase(String.valueOf(seat))) {
                return true;
            }
        }
        return false;
    }

    boolean isSelected(int seatNo) {
        int i;
        for (i = 0; i < filledSeats.size(); i++) {
            if (filledSeats.get(i).getmSeat_no().equalsIgnoreCase(String.valueOf(seatNo))) {
                return true;
            }
        }
        return false;
    }

    boolean isSelectedByUser(int seatNo) {
        int i;
        for (i = 0; i < selectedSeats.size(); i++) {
            if (selectedSeats.get(i).equalsIgnoreCase(String.valueOf(seatNo))) {
                return true;
            }
        }
        return false;
    }
}
