package com.example.jatistransproject;

import retrofit2.Call;
import retrofit2.http.GET;

public interface KeberangkatanInterface {
    String JSONURL = "http://10.0.2.2/jatistrans/";

    @GET("view.php")
    Call<String> getJSONString();
}
